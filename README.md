# Recto Decompiler

**Recto Decompiler** - модульный декомпилятор проектов, созданных на **PHP Devel Studio**

Recto Decompiler поддерживает большое число протекторов и их полный или частичный обход

### RectoDC API's

Recto Decompiler имеет большой функционал для создания собственных обходов и других полезных расширений. Так, декомпилятор состоит из:

1. Main API
2. System API
3. Encryptions API
4. GUI API
5. Main Menu API
6. Updates API

**Main API** - это основной API программы, является объектом и служит в качестве ядра для работы всего декомпилятора (Global $Decompiler)

**System API** - это API для работы с exemod, внутренними переменными и различными инструментами обработки кода (RDC_System)

**Encryptions API** - это API для работы с разными алгоритмами шифрования (RDC_Encryptions)

**GUI API** - это API для работы с GUI декомпилятора (RDC_GUI)

**Main Menu API** - это API для работы с главным меню декомпилятора (RDC_Menu)

**Updates API** - это API для работы с обновлениями декомпилятора (RDC_Updates)

### Расширения для обхода протекторов (на базе EPE)

```php
<?php

class MyRandomProtectorHook
{
	static function isDecompilable ()
	{
		if (RDC_System::getVar ('$YOU_CANT_DECMOPILE_THIS'))
			return true;

		else return false;
	}

	static function decompile ()
	{
		$res = gzinflate (RDC_System::getVar ('$YOU_CANT_DECMOPILE_THIS'));

		return array
		(
			'events'  => $res['events'],
			'forms'   => $res['forms'],
			'config'  => $res['config'],
			'modules' => $res['modules'],
			'res'     => serialize ($res)
		);
	}
}

?>
```

```php
<?php

// $hook - класс, описанный выше

EPE_BuildPacket ('MyHooks.epe', $hook, json_encode (array (
	'type'          => 'DecompilerModule.Protectors', // Тип расширения (здесь - для добавления своего обхода)
	'extClassName'  => 'MyRandomProtectorHook',       // Название класса
	'ProtectorName' => 'RandomProtector'              // Название протектора
)));

?>
```